from models import models
from app import app


def test_index():
    client = app.test_client()
    response = client.get('/')
    assert response.status_code == 200


def test_index_response():
    client = app.test_client()
    response = client.get('/')
    assert b"Employee Data" in response.data


def test_add():
    client = app.test_client()
    test_data = {'name': 'Mickey Test',
                 'gender': 'male',
                 'address': 'IN',
                 'phone': '0123456789',
                 'salary': '2000',
                 'department': 'Sales'}
    client.post('/add', data=test_data)
    assert models.Employee.query.count() >= 1


def test_delete():
    client = app.test_client()
    test_data = {'emp_id': 0}
    response = client.post('/delete', data=test_data)
    assert response.status_code == 200
    assert b"Sorry, the employee does not exist." in response.data
